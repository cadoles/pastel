# -*- coding: utf-8 -*-


class PastelError(Exception):
    pass


class PastelSSHError(IOError):
    pass


class PastelCfgError(Exception):
    pass
