# -*- coding: utf8 -*-
"logger, output and printing utilities"

import sys
import logging


def display_log_messages():
    "by now we just need a cmdline logging utility"
    logger = logging.getLogger("Pastel")
    sh = logging.StreamHandler()
    formatter = logging.Formatter('* %(asctime)s - [%(name)s] - %(levelname)s'
                                  ' - %(message)s')
    sh.setFormatter(formatter)
    logger.addHandler(sh)
    logger.setLevel(logging.INFO)
    return logger

logger = display_log_messages()

def pprint(msg):
    print("* %s" % msg)


def sprint(msg, prfx="[Pastel]"):
    sys.stdout.write("%s\t%s" % (prfx, msg))
