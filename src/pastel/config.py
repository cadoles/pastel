# -*- coding: utf-8 -*-

from yaml import load as Yload, dump as Ydump

from pastel.error import PastelCfgError


class PstConfig():
    """ Main configuration class
    """

    def __init__(self, configfile):
        self.configfile = configfile
        self.pastel_home = "/tmp"
        self.keydir = "/tmp/keys"
        self.keysize = 2048
        self.partnerfile = "/tmp/demo-parts"
        self.unitfile = "/tmp/demo-units"
        self.tr_chunksize = 10

    def load(self):
        """ Load configuration from file
        """
        data = file(self.configfile, 'r')
        obj = Yload(data)

        for elm in obj:
            try:
                getattr(self, elm)
                setattr(self, elm, obj[elm])
            except:
                msg = ("Configuration error : \"%s\" unsupported option" % elm)
                raise PastelCfgError(msg)

    def configfile_save(self):
        """ Save configuration to a file
        """
        # FIXME
        pass

    def partnerfile_set(self, partnerfile):
        """ Set a value for partfilefile
        
        :parram partnerfile: partners configuration file name, shall be a valid path
        """
        self.partnerfile = partnerfile


    def partnerfile_save(self, parteners):
        """ Save parteners file
        """
        data = None
        for part in parteners:
            data.append(part.to_json)
        json.dump(self.partnerfile)

    def unitfile_set(self, unitfile):
        """ Set a value for unitfile
        
        :param unitfile: transfert unit configuration file name
        """
        self.unitfile = unitfile

    def unitfile_save(self, units):
        """ Save unit file
        """
        data = None
        for unit in units:
            data.append(unit.to_json)
        json.dump(self.unitfile)
