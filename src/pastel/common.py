# -*- coding: utf8 -*-
"""Pastel main entry point
"""
import json
from os.path import isfile

from pastel.error import PastelError
from pastel.transfert import TransfertMonitor
from pastel.transfert import TransfertPartner
from pastel.transfert import TransfertUnit
from pastel.ui import pprint, logger


class Pastel():
    """ Main Object
    """
    def __init__(self, mainconf=None):
        self.partners = []
        self.units = []
        self.monitor = TransfertMonitor()
        if mainconf:
            self.conf = mainconf
        #FIXME Raise error if mainconf is missing

    def load_partners(self, partnerfile):
        """ Load all partners from a file
        """
        try:
            res = []
            # (first launch), if partnerfile does not exist it is created
            if not isfile(partnerfile):
                fh = file(partnerfile, 'w')
                fh.close()
            filehandle = file(partnerfile, 'r')

            try:
                obj = json.load(filehandle)
                for part in obj:
                    if "__class__" in part:
                        if part["__class__"] == "TransfertPartner":
                            prt = TransfertPartner(part["name"],
                                                   part["ip"],
                                                   part["user"],
                                                   part["password"],
                                                   part["dest_dir"],
                                                   self.conf.keydir,
                                                   self.conf.keysize,
                                                   part["private"],
                                                   part["public"])
                            res.append(prt)
            except ValueError as err:
                logger.error("Bad configuration file: {0} ".format(
                            partnerfile))
            self.partners = res
        except IOError as err:
            logger.error("Bad configuration file: {0} ".format(
                        partnerfile))
            raise PastelError(err)

    def save_partners(self, partnerfile):
        """ Write all partners to a file
        """
        stream = file(partnerfile, 'w')
        todump = []
        for part in self.partners:
            todump.append(part.to_json())
        json.dump(todump, stream)
        stream.close()

    def partner_exists(self, name, ip):
        """ Search for a partner
        """
        for part in self.partners:
            if part.ip == ip and part.name == name:
                return True
        return False

    def add_partner(self, name, ip, user, password, ddir):
        if self.partner_exists(name, ip) is False:
            if self.conf:
                pt = TransfertPartner(name, ip, user, password, ddir, 
                                      self.conf.keydir, self.conf.keysize)
            else:
                pt = TransfertPartner(name, ip, user, password, ddir)

            self.partners.append(pt)
        else:
            raise PastelError("Partner %s exists" % name)

    def list_partners(self):
        pprint("List of partners:")
        for part in self.partners:
            part.show("   ")

    def get_partner(self, name):
        for part in self.partners:
            if name == part.name:
                return part

        return None

    def load_units(self, unitfile):
        """ Load all units from a file
        """
        print("DEBUG")
        try:
            res = []
            # (first launch), if unitfile does not exist it is created
            if not isfile(unitfile):
                fh = file(unitfile, 'w')
                fh.close()
            filehandle = file(unitfile, 'r')

            try:
                obj = json.load(filehandle)
                for un in obj:
                    if "__class__" in un:
                        if un["__class__"] == "TransfertUnit":
                            unt = TransfertUnit(un["path"],
                                                   un["trid"],
                                                   un["size"],
                                                   un["name"])
                            res.append(unt)
            except ValueError as err:
                logger.error("Bad configuration file: {0} ".format(
                            unitfile))
            self.units = res
            print(res)
        except IOError as err:
            logger.error("Bad configuration file: {0} ".format(
                        unitfile))
            raise PastelError(err)

    def unit_exists(self, unitpath):
        for unit in self.units:
            if unit.path == unitpath:
                return True
        return False

    def add_unit(self, unitpath):
        if self.unit_exists(unitpath) is False:
            un = TransfertUnit(unitpath, self.conf.tr_chunksize)
            self.units.append(un)
        else:
            raise PastelError("Transfert Unit %s exists" % name)
    
    def list_units(self):
        pprint("List of transfert units : ")
        for unit in self.units:
            unit.show("   ")

    def save_units(self, unitfile):
        stream = file(unitfile, 'w')
        data = []
        for unit in self.units:
            data.append(unit.to_json())
        json.dump(data, stream)
        stream.close()

    def to_json(self):
        res = []
        for part in self.partners:
            data = part.to_json()
            res.append(data)
        return res
