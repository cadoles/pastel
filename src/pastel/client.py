# -*- coding: utf-8 -*-

import paramiko
from os.path import join, isfile, isdir
from pastel.error import PastelSSHError

class PstKey():
    """ A SSH RSA Key generator for pastel
    """

    def __init__(self, name, ip, keydir, keysize=2048):
        filename = "key-{0}-{1}".format(name, ip)
        self.private = join(keydir, "{0}.prv".format(filename))
        self.public = join(keydir, "{0}.pub".format(filename))

        if not isfile(self.private):
            key = paramiko.RSAKey.generate(keysize) 
            key.write_private_key_file(self.private)
            file = open(self.public,'w')
            file.write("ssh-rsa {0}== PastelKey\n".format(key.get_base64()))
            file.close()


class PstClient():
    """ A SSH client
    """
    def __init__(self, ip, user, password, rsakey=""):
        self.user = user
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            self.ssh.connect(ip, username=user,
                             key_filename=rsakey)
        except (paramiko.AuthenticationException, paramiko.SSHException) :
            print("\n\n\nOOOOOOOOOPPPPPPPSSS\n\n\n")
            try :
               self.ssh.connect(ip, username=user, password=password)
            except paramiko.AuthenticationException :
                raise PastelSSHError("Login Failed")

        self.sftp = None

    def runcmd(self, cmd):
        stdin, stdout, stderr = self.ssh.exec_command(cmd)
        stdin.close()
        return stdout.read().splitlines()

    def deploy_key(self, pubkey):
        try:
            self.sendfile(pubkey, "/tmp/key.pub")
            script = ["cat /tmp/key.pub >> ~{0}/.ssh/authorized_keys".format(
                       self.user)]
            script.append("rm /tmp/key")
            for cmd in script:
                print(self.runcmd(cmd))
        except PastelSSHError:
            raise PastelSSHError("Error deploying public key")

    def sendfile(self, file, dst):
        try:
            if self.sftp is None:
                self.sftp = self.ssh.open_sftp()
            self.sftp.put(file, dst)
            sum = self.runcmd('md5sum %s' % dst)
            return sum[0].split()[0]
        except IOError as err:
            msg = "Error during transfert of"
            raise PastelSSHError("%s %s to %s" % (msg, file, dst))

    def close(self):
        if self.sftp:
            self.sftp.close()
        self.ssh.close()
