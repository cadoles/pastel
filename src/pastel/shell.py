# -*- coding: utf-8 -*-

import cmd
from sys import stdin
from getpass import getpass

from pastel.ui import pprint, sprint, logger
from pastel.error import PastelError
from pastel.common import Pastel


class PastelShell(cmd.Cmd):
    """
    """
    def __init__(self, mainconf):
        cmd.Cmd.__init__(self)
        self.doc_header = "Documented commands (type help <command>:"
        self.undoc_header = "Undocumented commands"
        self.prompt = "#Pastel> "
        self.intro = """Pastel command line interpreter
        (type help or ? commands list)"""
        self.ruler = "-"
        self.pst = Pastel(mainconf)
        self.pst.load_partners(mainconf.partnerfile)
        self.pst.load_units(mainconf.unitfile)
        self.mainconf = mainconf
        self.alias = {'ls': 'list',
                      '+': 'add',
                      's': 'save',
                      'sv': 'save',
                      'q': 'exit',
                      'r': 'run',
                      '!': 'run'}

    def emptyline(self):
        print "Type 'exit' to finish withe the session or type ? for help."

    def default(self, line):
        cmd = line.split(' ')
        if cmd[0] in self.alias:
            meth = self.alias[cmd[0]]
            opts = line.split(" ", 1)
            if len(opts) <= 1:
                line = ""
            else:
                opts.pop(0)
                line = " ".join(opts)
            getattr(self, "do_" + meth)(line)
        else:
            print("Unkown command %s" % line)
            print("(type ’help’ for help for a list of valid commands)")

    def do_exit(self, line):
        """Exists from the console"""
        return True

    def do_EOF(self, args):
        """Exists on system end of file character"""
        return True

    def do_alias(self, line):
        """ List aliases """
        for alias in self.alias:
            pprint("%s\t%s" % (alias, self.alias[alias]))

    def do_list(self, line):
        """ List partners|xxx|xxx
        alias: ls
        example: list part
        """
        cmd = line.split(" ")
        if len(cmd) >= 1 and cmd[0]:
            if cmd[0] in ["part", "partener", "partenaire", "parts"]:
                self.pst.list_partners()
            elif cmd[0] in ["unit", "units", "unités", "unité"]:
                self.pst.list_units()
            else:
                print("Unsupported option %s" % cmd[0])
        else:
            print("Command list need options (type help list)")

    def do_save(self, line):
        """ save partner|xxx|xxx
        """
        cmd = line.split(" ")
        if len(cmd) >= 1 and cmd[0]:
            if cmd[0] in ["part", "partener", "partenaire", "parts"]:
                self.pst.save_partners(self.mainconf.partnerfile)
            elif cmd[0] in ["unit", "unités", "units", "unités"]:
                self.pst.save_units(self.mainconf.unitfile)
            else:
                print("Unsupported options %s" % str(cmd))
        else:
            print("Command save need options (type help save)")

    def add_part(self):
        sprint("Partner name: ")
        name = raw_input()
        sprint("Partner ip:")
        ip = raw_input()
        sprint("Partner user:")
        user = raw_input()
        password = getpass("Partner user password:")
        sprint("Partner destination directory:")
        ddir = raw_input()
        try:
            self.pst.add_partner(name, ip, user, password, ddir)
        except PastelError as err:
            logger.error(str(err))

    def add_unit(self):
        sprint("Transfert unit path: ")
        unitpath = raw_input()
        try:
            self.pst.add_unit(unitpath)
        except PastelError as err:
            logger.error(str(err))

    def do_add(self, line):
        """ Add partners|xxx|xxx
        command alias: +
        command example: add part
        """
        cmd = line.split(" ")
        if len(cmd) >= 1 and cmd[0]:
            if cmd[0] in ["part", "partener", "partenaire", "parts"]:
                self.add_part()
            elif cmd[0] in ["unit", "Unit", "file", "units", "unité"]:
                self.add_unit()
            else:
                print("Unsupported option %s" % cmd[0])
        else:
            print("Command add need options (type help add)")
        pass

    def do_run(self, line):
        """ Run command on a partner
        """
        cmd = line.split(" ")
        if len(cmd) >= 1 and cmd[0]:
            part = self.pst.get_partner(cmd[0])
            if part:
                pprint(part.name)
            else:
                pprint("No partner named {0}".format(cmd[0]))

        else:
            print("Command run need arguments (type help run)")
        pass
