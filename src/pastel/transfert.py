# -*- coding: utf-8 -*-

import os
import hashlib

from pastel.error import PastelError
from pastel.client import PstClient, PstKey
from pastel.ui import pprint, sprint, logger


class PstFile():
    """ A file
    """
    def __init__(self, path, trid=None, size=None, name=None):
        try:
            self.path = path
            self.data = ""
            if name:
                self.name = name
            else:
                self.name = os.path.basename(path)
            if size:
                self.size = size
            else:
                self.size = os.path.getsize(path)
            if trid :
                self.trid = trid
            else:
                self.get_trid()
        except OSError as err:
            raise PastelError(err)

    def get_trid(self):
        """ Create the trid (checksum of the file)
        """
        sum = hashlib.md5()
        filed = file(self.path, 'rb')
        while True:
            rdata = filed.read(1024)
            if len(rdata) == 0:
                break
            self.data += rdata
            sum.update(rdata)
        self.trid = sum.hexdigest()

    def get_data(self):
        self.get_trid()

    def show(self):
        pprint("%s\t\t%s\t\t%s" % (self.trid[0:12], self.path, self.size))


class TransfertUnit(PstFile):
    """ What you whant to upload
    """
    def __init__(self, path, trid=None, size=None, name=None, chunksize=10):
        PstFile.__init__(self, path)
        self.chunksize = chunksize
        self.metafile = "/tmp/" + os.path.basename(path) + ".mtd"
        self.nbchunks = self.size / self.chunksize
        self.chunks = []

    def write_meta(self):
        """ Write file metadata
        """
        metafile = open(self.metafile, 'w')
        line = self.path + ',' + 'chunk,' + str(self.nbchunks) + ','
        line += str(self.chunksize)
        metafile.write(line)
        metafile.close()

    def split(self, chunkdir="/tmp"):
        """ Split file and create chunks
        """
        self.get_data()
        self.write_meta()
        nm_cnt = 0
        for cnt in range(0, self.size + 1, self.chunksize):
            if nm_cnt <= 9:
                fn1 = "%s/%s.chunk0%s" % (chunkdir, self.name, nm_cnt)
            else:
                fn1 = "%s/%s.chunk%s" % (chunkdir, self.name, nm_cnt)
            open(fn1, 'wb').write(self.data[cnt:cnt + self.chunksize])
            self.chunks.append(PstFile(fn1))
            nm_cnt += 1

    def to_json(self):
        return {"__class__": "TransfertUnit",
                "trid": self.trid, 
                "path": self.path, 
                "name": self.name, 
                "size": self.size} 

    def show(self, prefix=""):
        pprint("%s%s" % (prefix, self.path))


class TransfertPartner():
    """ A transfert Partner
    """
    def __init__(self, name, ip, user, password,
                 dest_dir="/tmp/rcv",
                 kdir=None, ksize=2048,
                 priv=None, pub=None):
        self.name = name
        self.ip = ip
        self.user = user
        self.password = password
        self.session = None
        self.dest_dir = dest_dir
        if priv is None and pub is None:
            print(ksize)
            print(kdir)
            key = PstKey(self.name, self.ip, kdir, ksize)
            self.private = key.private
            self.public = key.public
        else:
            self.private = priv
            self.public = pub

    def open_session(self):
        self.session = PstClient(self.ip, self.user,
                                 self.password, self.private)
        self.session.deploy_key(self.public)

    def send_file(self, file, dst=None):
        if self.session is None:
            self.open_session()

        if dst:
            return self.session.sendfile(file, dst)
        else:
            return self.session.sendfile(file, file)

    def run(self, cmd):
        return self.session.runcmd(cmd)

    def to_json(self):
        return {"__class__": "TransfertPartner",
                "name": self.name,
                "user": self.user,
                "password": self.password,
                "ip": self.ip,
                "private": self.private,
                "public": self.public,
                "dest_dir": self.dest_dir}

    def show(self, prefix=""):
        pprint("%s%s\t: %s" % (prefix, self.name, self.ip))


class TransfertMonitor():
    """ Transfert Monitor
    """

    def __init__(self):
        self.units = []

    def add_unit(self, unit):
        unit = TranfertUnit(path)
        unit.split()
        self.units.append(unit)

    def load_unit(self, unit):
        unit.split()
        self.units.append(unit)

    def list_units(self):
        pprint("ID\t\tPath\t\tSize\t\t")
        for unit in self.units:
            unit.show()

    def do_transfert(self, partner):
        dst = partner.dest_dir
        for unit in self.units:
            pprint("Sending %s:" % unit.path)
            for chunk in unit.chunks:
                logger.info("   Tranfert of {0}".format(chunk.path))
                res = partner.send_file(chunk.path, "{0}/{1}".format(dst,
                                                               chunk.name))
                if res != chunk.trid:
                    logger.error("ERROR on transfert of {0}".format(chunk.path))
                else:
                    logger.info("   [SUCCESS]")
            pprint("Join of %s" % unit.name)
            partner.run("cat %s/%s.chunk* > %s/%s" % (dst,
                                                      unit.name,
                                                      dst,
                                                      unit.name))

    def show(self):
        for unit in self.units:
            if unit.chunks:
                for chunk in unit.chunks:
                    chunk.show()
